/**
 * @author kun
 *
 */
module com.easy.assembly.logging {

	exports com.easy.assembly.logging;

	requires transitive ch.qos.logback.classic;
	requires transitive ch.qos.logback.core;
	requires transitive java.management;
	requires org.slf4j;
}