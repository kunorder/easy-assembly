package com.easy.assembly.logging;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

/**
 * 加载配置文件
 * 
 * @author kun
 *
 */
public class LoadConfiguration {

	/**
	 * 加载配置文件
	 * 
	 * @param logConfig 配置文件路径
	 * @throws JoranException 加载异常
	 */
	public static void loading(String logConfig) throws JoranException {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		JoranConfigurator configurator = new JoranConfigurator();
		configurator.setContext(loggerContext);
		loggerContext.reset();
		configurator.doConfigure(LoadConfiguration.class.getClassLoader().getResourceAsStream(logConfig));
		StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);
	}

}
