# easy-assembly

* JDK9
* logback 日志

## easy-logging

* logback 简易集成
* ANSI 支持

### 使用方式

```
 <repositories><!-- 代码库 -->
		<repository>
			<id>git-maven</id>
			<url>https://gitee.com/zjkorder/maven-repository/raw/master/repository</url>
		</repository>
 </repositories>
```

```
		<dependency>
			<groupId>com.easy.assembly</groupId>
			<artifactId>easy-logging</artifactId>
			<version>0.0.1-SNAPSHOT</version>
		</dependency>

```